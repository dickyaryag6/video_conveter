FROM ubuntu:20.04

#ENV PYTHONUNBUFFERED 1

RUN mkdir /app

WORKDIR /app

COPY . /app/

RUN apt-get update && apt-get install -y \
    software-properties-common
RUN add-apt-repository universe

RUN apt-get install ffmpeg -y

RUN apt-get install \
    python3.7 -y\
    python3-pip -y

RUN pip3 install --default-timeout=10000 --upgrade pip

RUN pip3 install -r requirements.txt

#install ffmpeg

#RUN apt-get install ffmpeg

#CMD ["python", "manage.py", "runserver 127.0.0.1:8001"]
