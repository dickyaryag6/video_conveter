# Video Convertion API

form data need to be submitted to API:
`video_file`, `desired_format`, `desired_bitrate`,`desired_resolution`

for example : <br/>
`video_file` : `video_file.mp4` <br/>
`desired_format` : `avi` <br/>
`desired_bitrate` : `350K` <br/>
`desired_resolution` : `640x480` <br/>

form send to http://host:port/api/converter/, after that you are going to get the response like this : <br/>
```json
{
  "message": "converted",
  "donwload_link": "127.0.0.1:8001/tmp/example-1597217815.994457.avi"
}
```

visit the link given by the json response to download the converted video

visit https://hub.docker.com/repository/docker/dickyarya/videoconverter-dicky to get the docker image