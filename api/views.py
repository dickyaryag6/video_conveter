
import os
import subprocess
import time

from django.http import JsonResponse

from django.views.generic import View

from api.forms import VideoConverterForm


def handle_uploaded_files(f):
    with open('api/tmp/' + f.name, 'wb+') as destination:
        for chunk in f.chunks():
            destination.write(chunk)
    # change_name
    file_name = os.path.splitext(f.name)[0]
    file_format = os.path.splitext(f.name)[1]

    now = time.time()
    os.rename(r'api/tmp/{}'.format(f.name), r'api/tmp/{}-{}{}'.format(file_name, now, file_format))

    return "api/tmp/{}-{}{}".format(file_name, now, file_format)


def convert_video(desired_format, desired_bitrate, desired_resolution, file_name):
    file_name_wo_format = os.path.splitext(file_name)[0]

    conversion_command = "ffmpeg -i {} -s {} -b:v {} {}.{}".format(
        file_name,
        desired_resolution,
        desired_bitrate,
        "tmp/"+file_name_wo_format.split("/")[-1],
        desired_format
    )

    print("command : ", conversion_command)
    converted_filename = "{}.{}".format("tmp/"+file_name_wo_format.split("/")[-1], desired_format)

    returned_value = subprocess.call(conversion_command, shell=True)  # returns the exit code in unix

    # hapus file original
    os.remove(file_name)
    return returned_value, converted_filename


# def download_file(request):

class VideoConverterView(View):
    def post(self, request, *args, **kwargs):
        if request.method == "POST":
            form = VideoConverterForm(request.POST, request.FILES)
            if form.is_valid():
                # save video file
                file_name = handle_uploaded_files(request.FILES["video_file"])
                desired_format = form['desired_format'].value()
                desired_bitrate = form['desired_bitrate'].value()
                desired_resolution = form['desired_resolution'].value()

                # cek bitrate
                if desired_bitrate.find("M") == -1 and desired_bitrate.find("K") == -1:
                    print("bitrate")
                    return JsonResponse({'message': 'failed'}, status=400)
                # cek resolution
                if desired_resolution.find("x") == -1:
                    return JsonResponse({'message': 'failed'}, status=400)
                # convert video
                result, converted_filename = convert_video(desired_format,
                                                           desired_bitrate,
                                                           desired_resolution,
                                                           file_name
                                                           )
                if result == 0:
                    return JsonResponse({'message': 'converted', 'donwload_link': request.get_host()+"/"+converted_filename}, status=200)
                else:
                    return JsonResponse({'message': 'failed'}, status=400)
        else:
            return JsonResponse({'message': 'failed'}, status=400)
