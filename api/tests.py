from django.core.files.uploadedfile import SimpleUploadedFile
from django.test import TestCase
from rest_framework import status
from rest_framework.test import APITestCase

from api.forms import VideoConverterForm


class CallAPITests(APITestCase):
    upload_file = open('api/tmp/example.mp4', 'rb')

    file_dict = {'video_file': SimpleUploadedFile(upload_file.name, upload_file.read())}
    form = VideoConverterForm(data={
        "desired_format": "avi",
        "desired_bitrate": "350K",
        "desired_resolution": "640x480"
    }, files=file_dict)

    url = "/api/converter/"

    def test_form_with_data(self):
        self.assertTrue(self.form.is_valid())

    def test_form_with_no_data(self):
        form = VideoConverterForm({})
        self.assertFalse(form.is_valid())

    def test_post(self):
        response = self.client.post(self.url, self.form, content_type="multipart/form-data")
        self.assertEqual(response.status_code, status.HTTP_200_OK)



