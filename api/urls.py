from django.conf.urls import url
from .views import VideoConverterView


urlpatterns = [
    # url(r'^admin/', admin.site.urls, name='admin'),
    url(r'^converter/', VideoConverterView.as_view(), name='videoconveter'),
]