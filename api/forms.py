from django import forms


class VideoConverterForm(forms.Form):
    # video file
    video_file = forms.FileField()
    # desired format
    desired_format = forms.CharField()
    # desired bitrate
    desired_bitrate = forms.CharField()
    # desired resolution
    desired_resolution = forms.CharField()
